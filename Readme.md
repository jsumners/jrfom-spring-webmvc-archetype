# jrfom-spring-webmvc Archetype #

This project provides the necessary structure to create a Maven archetype. This
archetype generates a minimal annotation based [Spring](http://spring.io/) web
application.

## Usage ##

### CLI Maven ###

To use the archetype from the command line Maven interface, you simply specify
the release repository when issuing a `archetype:generate`. For example:

```
mvn archetype:generate -DarchetypeCatalog="https://github.com/jsumners/mvn-repo/raw/master/releases/"
```

### IntelliJ ###

When creating a new project in IntelliJ, and specifying the "Maven module", you
can simply fill in the [add archetype dialog](http://www.jetbrains.com/idea/webhelp/add-archetype-dialog.html)
with the following values:

1. GroupId = "com.jrfom"
2. ArtifactId = "jrfom-spring-webmvc"
3. Version = "RELEASE" (or "0.6")
4. Repository = "https://github.com/jsumners/mvn-repo/raw/master/releases/"

## Spring Profiles ##

The skeleton project created by this archetype defines two Spring profiles:
"dev" and "prod" ("development" and "production"). As a result, the produced
application *will not* load unless a "application-dev.properties" or
"application-prod.properties" is in the classpath (for development and
production, respectively). Empty files for each are automatically generated in
the "resources" directory.

Further more, the default profile is "prod". To use the development profile,
you must define the system property "application.profile" to "dev". The easiest
way to do this is via a `-D` paramter. For example:

```
java -Dapplication.profile=dev
```

The by-product of this is that you can define any profile name via this system
property and the application will look for a properties file that includes that
name, e.g. `application.profile=foo` would require a `application-foo.properties`
file to be on the classpath.